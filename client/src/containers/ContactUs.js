import React, {useState} from 'react';
import Splash from '../images/contact.jpg';
import {Form, Button} from 'semantic-ui-react';
import {NavLink} from 'react-router-dom';
import axios from '../config.js';

const ContactUs = (props) => {
	const [form, setForm] = useState({
		name:"",
		email:"",
		message:""	
	});
	
	const handleChange = (event) => {
		setForm({ ...form, [event.target.name]: event.target.value});
	};


	//Post email request to server
	const handleSubmit = (event) => {
		event.preventDefault(); 
		var data = {name:form.name, email:form.email, message:form.message, subject:`message from ${form.name} at ${form.email}`}

		axios.post('/emails/send_email', data)
		.then((response) => {
			setForm({ name:"", email:"", message:""	})
			alert("Your message has been sent, thanks!")
		})
		.catch(function (error) {
			console.log(error);
		})
	}
	
	return (
		<>
			{/*** SPLASH ****/}	
			<img src={Splash} alt="Fern in hands background" id="splashImage" />
			
			<section className="section">

				{/*** Overlapping Box ****/}
				<div className="overlappingBox">
					<h2>WE LOVE THAT YOU HAVE QUESTIONS!</h2>
					<p>Have a visit to our frequently asked questions page and you might get your answer straight away! </p>
					<NavLink exact to={'/faq'}>
						<Button color="brown" size="big">FAQ</Button>
					</NavLink>
				</div>

				{/*** Whitespace Container ****/}
				<div className="whitespaceContainer">
					<h2>WE ARE HERE FOR YOU!</h2>
					<p>Please find a contact form below, we strive to get back to you within 24 hours. You can always email us at info@rainbowbeetroot.com or give us a call at 08-555 555 52.</p>
				</div>

				{/*** Form Container ****/}
				<Form onSubmit={handleSubmit} onChange={handleChange} className="formContainer">
					<Form.Group widths='equal'>
						<Form.Input  label="Name" placeholder='What is your name?' name="name" value={form.name} />
						<Form.Input  label="Email" placeholder='Your contact email?' name="email" value={form.email}/>
					</Form.Group>
					<Form.TextArea  style={{minHeight: "20vh"}} label="Message" placeholder='Please write your message' name="message" value={form.message}/>		
					<Button color="brown">Send Message</Button>	
				</Form>
			</section>
		</>
	)
}

export default ContactUs;
