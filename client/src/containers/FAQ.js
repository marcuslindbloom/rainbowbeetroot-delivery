import React from 'react';

const FAQ = (props) => {



	return (
		<>
			<section className="section">

				
				{/*** Whitespace Container ****/}
				<div className="whitespaceContainer">
					<h2>FREQUENTLY ASKED QUESTIONS</h2>
					<p>When trying something new we know that some questions always come up, we have listed the most common ones below.</p>
				</div>	
				
				{/*** Highlight Container ****/}
				<div className="highlightContainer">
					<h3> When do you deliver? </h3>
					<p>	We deliver on Sundays and we will send you the day before our delivery schedule for your address.</p>
					<h3> How do you deliver? </h3>
					<p> We deliver by electric car, with all groceries packed in paperbags outside your doorstep. </p>
					<h3> What if something is missing?</h3> 
					<p> Please contact us on the telephone number on the delivery note, we will replace the missing item within 24 hours of notice</p>
					<h3> I want to make changes to my order</h3> 
					<p> Please contact us, we accept order changes prior 16.00 on Friday before delivery</p>
					<h3> I want to cancel my order</h3> 
					<p> Please contact us, we accept cancellations free of charge prior 16.00 on Friday before delivery and we will refund you within 7 business days</p>
				</div>
				
				{/*** Whitespace Container ****/}
				<div className="whitespaceContainer">
					<h2>IS YOUR QUESTIONS NOT LISTED?</h2>
					<p>Please contact us via email, telephone or through our contact us page and we will get back to your within 24 hours.</p>
				</div>	
			</section>
		</>
	
	)
} 

export default FAQ;
