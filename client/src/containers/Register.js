import React, { useState } from 'react';
import axios from '../config.js';
import { Form, Button} from 'semantic-ui-react';

const Register = (props) => {
	const [ form, setValues ] = useState({
		email: '',
		password: '',
		password2: '',
		address: '',
		phone: '',
		firstName:'', 
		lastName:''

	});
	const [ message, setMessage ] = useState('');
	const [displayMessage, setDisplayMessage] = useState(false);


	const handleChange = (e) => {
		setValues({ ...form, [e.target.name]: e.target.value });
	};

	const handleSubmit = async (e) => {
		e.preventDefault();
		try {
			const response = await axios.post(`/users/register`, {
				firstName:form.firstName, 
				lastName:form.lastName, 
				email: form.email,
				password: form.password,
				password2: form.password2,
				address: form.address,
				phone: form.phone

			});
			setMessage(response.data.message);
			setDisplayMessage(true);
			if (response.data.ok) {
				setTimeout(() => {
					props.history.push('/login');
				}, 2000);
			}
		} catch (error) {
			console.log(error);
		}
	};

	return (
		<>
		<section className="section">
			
			{/*** Whitespace Container ****/}
			<div className="whitespaceContainer">
				<h2>Register a new account </h2>
				<p>Please enter your details below.</p>
			</div>
				
			{/*** Register Form ****/}		
			<Form onSubmit={handleSubmit} onChange={handleChange} className="formContainer">
				<Form.Group widths='equal'>
					<Form.Input label="First Name" placeholder="Your first name?" name="firstName" />
					<Form.Input label="Last Name" placeholder="Your last name?" name="lastName" />
				</Form.Group>
				<Form.Group widths='equal'>
					<Form.Input label="Email" placeholder="What is your email address?" name="email" />
					<Form.Input label="Telephone" placeholder="What is your phone number?" name="phone" />
				</Form.Group>
				<Form.Group widths='equal'>
					<Form.Field>
						<label>Password</label>
						<Form.Input type="password" placeholder="Enter your password" name="password" />
					</Form.Field>
					<Form.Field>
						<label>Confirm Password</label>
						<Form.Input type="password" placeholder="Confirm your password" name="password2" />
					</Form.Field>
				</Form.Group>						
				<Form.Input label="Address" placeholder="What is your address?" name="address" />
				<Button color="brown">Register</Button>
				{displayMessage? <div className="messageBox"><h3>{message}</h3></div>:null}
			</Form>
		</section>
		</>
	);
};

export default Register;
