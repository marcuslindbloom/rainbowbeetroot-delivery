import React, { useEffect, useState } from 'react';
import axios from '../config.js';
import { Form, Button } from 'semantic-ui-react';
import ButtonToggle from '../components/ButtonToggle'; 
import { useRecoilState } from 'recoil';
import { cartNumber, clearButtons } from '../state';
import { NavLink } from 'react-router-dom';


const SingleProduct = (props) => {

	const [product, setProduct] = useState({});
	const [, setCartItemCount] = useRecoilState(cartNumber);
	const [, setClear] = useRecoilState(clearButtons);
	const [inputValues, setInputValues] = useState({
		mealsPerWeek:0,
		peoplePerWeek:0,
		amountOfWeeks:0
	})
	useEffect(() => {
		getProduct();
	},[]) // eslint-disable-line react-hooks/exhaustive-deps


	const getProduct = () => { 
		axios.post(`/products/find_one`, {name:props.match.params.product})
		.then((res) => {
			setProduct(res.data.product);
		})
		.catch((error) => {
			console.log(error);
		})
	} 

	// Rendering specific value buttons for ordering 
	const numberedButtons = (min, max) => {
		let arr = [];	
		for(let i = min;i<=max;i++) {
			arr.push(i); 
		}
		return(arr.map((value, idx) => (<ButtonToggle value={value} key={idx}/>)
		))
	} 
	
	const handleChange = (event) => {
		setInputValues({ ...inputValues, [event.currentTarget.id]: !isNaN(event.target.innerText)?parseInt(event.target.innerText):event.target.innerText });
	};

	const handleSubmit = (event) => {
		event.preventDefault();

		let price = product.price;
		let order = {product:event.target.name, mealsPerWeek:inputValues.mealsPerWeek, peoplePerWeek:inputValues.peoplePerWeek, amountOfWeeks:inputValues.amountOfWeeks, price:price};
		let cart = JSON.parse(localStorage.getItem('cart'));
		setClear({});
		if(order.mealsPerWeek !== 0 && order.peoplePerWeek !== 0 && order.amountOfWeeks !== 0) { 
			if(!cart) {
				localStorage.setItem('cart', JSON.stringify([order]));	
				setCartItemCount(1);
			} else { 
				localStorage.removeItem('cart');
				cart = [...cart];
				cart.push(order);
				setCartItemCount(cart.length);
				localStorage.setItem('cart', JSON.stringify(cart));	
			}
			setInputValues({mealsPerWeek:0, peoplePerWeek:0, amountOfWeeks:0});
		} else { 
			alert("Please ensure all fields are filled before adding to cart");
		}
	} 


	return (
		<>
			<div id="product">
			<section className="section">

				{/*** Grid Wrapper ****/}
				<div className="gridWrapper">
					<h2>{props.match.params.product}</h2>
					<p>{product.longDescription} </p>
				</div>
					<div className="gridAlternating">
						<>
						<img src={product.image} alt="product related" className="gridImg" />
						<div className="productTextWrapperLeft">
							<>
							<Form onSubmit={handleSubmit} name={product.name}>
								<p>Price / person / meal: {product.price} sek</p>
								<Form.Field>
									<label>How many meals per week?</label>
									<Button.Group id="mealsPerWeek" onClick={handleChange}>
										{numberedButtons(product.minMeals, product.maxMeals)}
									</Button.Group>	
								</Form.Field>
								<Form.Field>
									<label>How many persons?</label>
										<Button.Group id="peoplePerWeek" onClick={handleChange}>
							     		{numberedButtons(2,6)}
									</Button.Group>
								</Form.Field>
								<Form.Field>
									<label>How many weeks would you like to order for?</label>
										<Button.Group id="amountOfWeeks" onClick={handleChange}>
							     		{numberedButtons(1,4)}
							     		{/*<Button type="button">Subscription</Button>*/}
									</Button.Group>
								</Form.Field>
								<div>
									<Button type='submit'>Add to Cart</Button>
									<NavLink exact to={'/products'}>	
										<Button type='button'>Back to Products</Button>
									</NavLink>
								</div>
							</Form>			
							</>
						</div>
						</>				
				</div>
			</section>
			</div>
		</>
	)
}

export default SingleProduct;
