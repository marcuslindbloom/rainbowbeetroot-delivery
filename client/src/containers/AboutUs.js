import React from 'react';
import Splash from '../images/Aboutus.jpg';

const AboutUs = (props) => {



	return (
		<>
			{/*** SPLASH ****/}	
			<img src={Splash} alt="Food background" id="splashImage" />
			
			<section className="section">
				{/*** Whitespace Container ****/}
				<div className="whitespaceContainer">
					<h2>WHO ARE WE?</h2>
					<p>Starting out at a small family business, we currently deliver about 2000 portions of food on a weekly basis. We have always loved the idea of healthy food providing a happy life. Too few today have the time to prepare nourishing food for the family or for themselves, that is our mission.</p>
				</div>

				{/*** Highlight Container ****/}
				<div className="highlightContainer">
					<h2>VISION</h2>
					<p>Be the top of mind grocery delivery company in Sweden by 2024</p>
					<h2>MISSION</h2>
					<p>Prepare nourishing food and recipes for those who do not have time to do it themselves</p>
					<h2>VALUES</h2>
					<p>We always deliver the best healthy foods to our customers</p>
					<p>We always reduce our environmental footprint in the world</p>
					<p>We always makes sure our products gets to the right place at the right time</p>
				</div>
				<div className="whitespaceContainer">
					<h2>OUR HISTORY</h2>
					<p>The idea formed in 2018, whilst we were ourselves overworked and were struggling on a daily basis to put healthy food on the family dining table in a timely manner. We spent too little time with family and too much time with daily shores. So the idea was born, we started out in small scale from our own kitchen. Quickly we picked up pace and we were able to rent an industrial kitchen. </p>
					<p>Since then we have backing of several startup investors and we now have a fleet of electric cars with cooling/freezing facilities and 20 employees. We are focused on establishing our company in the other large cities in Sweden over the next couple of years. It's a start of a great journey.</p>
				</div>


			</section>
		</>
	)
}

export default AboutUs;
