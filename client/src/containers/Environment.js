import React from 'react';
import Splash from '../images/electric.jpg';
import {NavLink} from 'react-router-dom';
import {Button} from 'semantic-ui-react';
const Environment = (props) => {



	return (
		<>	
			{/*** SPLASH ****/}	
			<img src={Splash} alt="Food background" id="splashImage" />
			
			<section className="section">
				
				{/*** Overlapping Box ****/}
				<div className="overlappingBox">
					<h2>BE A PART OF OUR TEAM?</h2>
					<p>If you are interested in being a part of our team, contact us and tell us what you want to do to save the environment we live in.</p>
					<NavLink exact to={'/contact-us'}>
						<Button color="brown" size="big">Contact Us</Button>
					</NavLink>
				</div>

				{/*** Whitespace Container ****/}
				<div className="whitespaceContainer">
					<h2>RAINBOW BEETROOT & ENVIRONMENT</h2>
					<p>We work on an everyday basis to improve our practices in our to reduce our environmental impact. Below we have outlined our most important initiatives for 2021.</p>
				</div>

				{/*** Highlight Container ****/}
				<div className="highlightContainer">
					<h2>ELECTRIC DELIVERY</h2>
					<p>Our whole delivery fleet is now electrified and we are powering our vechiles only with renewable energi from sun, wind and water.</p>
					<h2>PLASTIC FREE</h2>
					<p>We are completely plastic free as of 2021 and deliver only in paperbags, paper packaging and wood.</p>
					<h2>FOODSHELTER DELIVERY</h2>
					<p>For every 10 meals we deliver we donate one meal to a food shelter in central stockholm. </p>
				</div>
				
				{/*** Whitespace Container ****/}
				<div className="whitespaceContainer">
					<h2>DO YOU HAVE IDEAS FOR US?</h2>
					<p>We believe that the sharing of ideas is the solution for the global climate crisis and please feel free to send us any ideas you might have on how we can improve our way of business in order to reduce our environmental impact.</p>
				</div>
 

			</section>
		</>
	)
}

export default Environment;
