import React,{useState} from 'react';
import { Button } from 'semantic-ui-react';
import UserData from '../components/UserData';
import axios from '../config.js';


const UserProfile = (props) => {
	const [orders, setOrders] = useState({});
	const [displayProd, setDisplayProd] = useState(false);

	const getEmail = (email) => {

		//Email retrieved from UserData, get orders for User
		if(typeof email !== "undefined") { 
			axios.post(`/orders/find_user_orders`,{inputEmail:email})
			.then((res) => {
				setOrders(res.data.orders);
				setDisplayProd(true);
			})
			.catch((error) => {
				console.log(error);
			})
		}
	}


	const renderOrders = () => { 
		return orders.map((ord,idx) => {
			let ordDate = ord.orderDate.split('T');
			let delDate = ord.initialDeliveryDate.split('T');

			
			return(		
			<div className="orderWrapper" key={idx}>
				<h4>Order ID: {ord._id}, Total Price: {ord.totalPrice} sek, Order date: {ordDate[0]}, Delivery date: {delDate[0]}</h4>
				{renderProducts(ord.products)}
			</div>
			)
		})
	}

	const renderProducts = (arr) => {
		return arr.map((prod, idx) => {
			return (
				<div className="gridOrder" key={idx}>
					<p>{prod.product}</p>
					<p>Meals per week: {prod.mealsPerWeek}</p>
					<p>People per meal: {prod.peoplePerWeek}</p>
					<p>Amount of weeks: {prod.amountOfWeeks}</p>
					<p>Item price: {prod.price*prod.mealsPerWeek*prod.peoplePerWeek*prod.amountOfWeeks} sek</p>	
				</div>
			)	
		})
	}


	return (
		<>
		<section className="section">
			{/*** Whitespace Container ****/}
			<div className="whitespaceContainer">
				<h2>My Page</h2>
				<p>Below you can find details of your account.</p>
			</div>
			
			<UserData getEmail={getEmail}/>
			<div className="boxContainer">
				<h2>Order History</h2>	
				{displayProd?renderOrders():null}
			</div> 
			<Button color="brown" onClick={() => {
					props.history.push('/');
					props.logout();
				}}> 
				Logout 
			</Button>
		</section>
		</>
	);
};

export default UserProfile;
