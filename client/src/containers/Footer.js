import React from 'react';

const Footer = (props) => {



	return (
		<div className="footer">
			<div className="footerWrapper">
				<h2>Follow us</h2>
				<p>Twitter</p>
				<p>Instagram</p>
				<p>Facebook</p>
			</div>
			<div className="footerWrapper">
				<h2>Contact us</h2>
				<p>Hornsgatan 15 12526, Stockholm, Sweden</p>

				<p>08-555 555 52</p>
				<p>info@rainbowbeetroot.com</p>
					
			</div>
				<div className="footerWrapper">
				<h2>Information</h2>
				<p>About us</p>
				<p>Career</p>
				<p>Integrity Policy</p>
				<p>Terms & Conditions</p>
			</div>
		</div>
	)
} 

export default Footer;