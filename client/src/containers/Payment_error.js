import React from 'react';
import {NavLink} from 'react-router-dom';
import {Button} from 'semantic-ui-react';

const PaymentError = props => {
  return (
    <>
      <section className="section">
        {/*** Whitespace Container ****/}
          <div className="whitespaceContainer">
              <h2>PAYMENT ERROR</h2>
              <p>An error occured during the payment process, the transaction was not approved. If you have any questions, please do not hesitate to contact us. If you wish to try again, please go to the Cart to continue checkout.</p>
          </div>

          <NavLink exact to={'/cart'}>
              <Button color="brown" size="big">Back to Cart</Button>
          </NavLink>
      </section>
    </>
  );
};

export default PaymentError;
