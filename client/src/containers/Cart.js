import React, { useState, useEffect } from 'react';
import { useSetRecoilState } from 'recoil';
import { cartNumber } from '../state';
import { Button, Icon } from 'semantic-ui-react';
import axios from '../config.js';
import { injectStripe } from "react-stripe-elements";
import UserData from '../components/UserData';
import { NavLink } from 'react-router-dom'; 
import Daypicker from '../components/Daypicker'


const Cart = (props) => {
	
	const [productsInCart, setProductsInCart] = useState([]);
	const setCartItemCount = useSetRecoilState(cartNumber);
	const [totalPrice, setTotalPrice] = useState(0);
	const [userData, setUserData] = useState({});
	const [productsToStripe, setProductsToStripe] = useState([]);
	const [displayProd, setDisplayProd] = useState(false);


	useEffect(() =>{
		const updateCart = () => {
			let cart = JSON.parse(localStorage.getItem('cart'));
			if(!cart) {  
				setCartItemCount(0)
				setDisplayProd(false);
			} else { 
				setCartItemCount(cart.length);
				setProductsInCart(cart);
				calculateTotals(cart);
				setDisplayProd(true);
			}			
		}
		updateCart();
		fetchUserData();
		localStorage.setItem('date', JSON.stringify(''));
	}, []) // eslint-disable-line react-hooks/exhaustive-deps
	
	useEffect(() =>{
		updateProductsToStripe();

	},[productsInCart]) // eslint-disable-line react-hooks/exhaustive-deps


// =============== USERINFO FOR PAYMENT ===============  

	const fetchUserData = async (props) => {
		const token = JSON.parse(localStorage.getItem('token'));
		try {
			axios.defaults.headers.common['Authorization'] = token;
			const response = await axios.post(`/users/get_user_info`);
			setUserData(response.data);
		} catch (error) {
			console.log(error);
		}
	}

// =============== RENDERING FUNCTIONALITY ===============  

	const renderCart = (products) => {
		return(
		products.map((prod, idx) => ( 
			<div className="cartItemGrid" key={idx}>
				<p>{prod.product}</p>
				<p>Meals per week: {prod.mealsPerWeek}</p>
				<p>People per meal: {prod.peoplePerWeek}</p>
				<p>Amount of weeks: {prod.amountOfWeeks}</p>
				<p>Item price: {prod.price*prod.mealsPerWeek*prod.peoplePerWeek*prod.amountOfWeeks} sek</p>	
				<Icon name='delete' size="big" className="deleteButton" onClick={() => handleRemove(idx)} />
			</div>
	)))};

	// Calculating total price for each product
	const calculateTotals = (products) => { 
		let tPrice = 0;
		for(var prod of products) { 
			tPrice += prod.price * prod.mealsPerWeek * prod.peoplePerWeek * prod.amountOfWeeks;
		}
		setTotalPrice(tPrice);
	}

 	const handleClick = (event) => { 
		let date = JSON.parse(localStorage.getItem('date'));
		if(date === '') { 
			alert("Please select a delivery date");
		} else {
			createCheckoutSession();
		}		
	}

	const handleRemove = (idx) => {
		let products = [...productsInCart];
		products.splice(idx, 1);
		localStorage.setItem('cart', JSON.stringify(products));
		updateCart();
	}

	const emptyBasket = () => {
		localStorage.removeItem('cart');
		setCartItemCount(0);
		setProductsInCart([]);
		setTotalPrice(0);
	}

	// Function to clear error with userData component
	const getEmail = () => {}

	const updateCart = () => {
		let cart = JSON.parse(localStorage.getItem('cart'));
		if(!cart) {  
			setCartItemCount(0)
		} else { 
			setCartItemCount(cart.length);
			setProductsInCart(cart);
			calculateTotals(cart);
		}			
	}


// =============== STRIPE PAYMENT ===============  
	const createCheckoutSession = async () => {
	    let products = [...productsToStripe];
	    let email = userData.email;
	    try {
	      const response = await axios.post(
	        "/payment/create-checkout-session",
	        { products, email }
	      );
	      return response.data.ok
	        ? (localStorage.setItem(
	            "sessionId",
	            JSON.stringify(response.data.sessionId)
	          ),
	          redirect(response.data.sessionId))
	        : props.history.push("/payment/error");
	    } catch (error) {
          console.log({error})
	      props.history.push("/payment/error");
	    }
	};

	const redirect = sessionId => {
	    props.stripe
	      .redirectToCheckout({
	        // Make the id field from the Checkout Session creation API response
	        // available to this file, so you can provide it as parameter here
	        // instead of the {{CHECKOUT_SESSION_ID}} placeholder.
	        sessionId: sessionId
	      })
	      .then(function(result) {
	        // If `redirectToCheckout` fails due to a browser or network
	        // error, display the localized error message to your customer
	        // using `result.error.message`.
	      });
	 };

	const updateProductsToStripe = () => { 
		let arr = [];
		for(let prod in productsInCart) { 
			arr.push({name:productsInCart[prod].product, quantity:1, amount:productsInCart[prod].price*productsInCart[prod].mealsPerWeek*productsInCart[prod].peoplePerWeek*productsInCart[prod].amountOfWeeks});
		}
		setProductsToStripe(arr);
	}

	//Get delivery date from Daypicker
	const getDate = (date) => { 
		localStorage.setItem('date', JSON.stringify(date));
	}

	return (
		<>
		<section className="section">
			{/*** Whitespace Container ****/}
			<div className="whitespaceContainer">
				<h2>Shopping Cart </h2>
				<p>Please find the contents of your cart below.</p>
			</div>
				
			{/*** Grid Cart ****/}		
			{displayProd?<div className="gridCart">{renderCart(productsInCart)}</div>:null}
			<UserData getEmail={getEmail} />
			{/*** Whitespace Container ****/}
			<div className="whitespaceContainer">
				<h2>Select first delivery date </h2>
				<Daypicker getDate={getDate} />
			</div>
			
			<h3>Total Price: {totalPrice} sek</h3>
			<div id="buttonsCart">
				<Button color="brown" type="button" onClick={handleClick}>Checkout</Button>
				<Button color="brown" type="button" onClick={emptyBasket}>Clear Basket</Button>	
				<NavLink exact to={'/products'}>	
					<Button color="brown" type='button'>Back to Products</Button>
				</NavLink>
			</div>
		</section>
		</>
	);
};

export default injectStripe(Cart);
