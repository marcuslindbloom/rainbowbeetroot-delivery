import React, { useState, useEffect } from 'react';
import axios from '../config.js';
import { Form, Button } from 'semantic-ui-react';
import { useRecoilState } from 'recoil';
import { cartNumber, clearButtons } from '../state';
import ButtonToggle from '../components/ButtonToggle'
import { NavLink } from 'react-router-dom';


const Addons = (props) => {

	const getWidth = () => (window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth);
	const [products, setProducts] = useState({});
	const [displayProd, setDisplayProd] = useState(false);
	const [width, setWidth] = useState(getWidth());
	const [, setCartItemCount] = useRecoilState(cartNumber);
	const [, setClear] = useRecoilState(clearButtons);
	const [inputValues, setInputValues] = useState({
		mealsPerWeek:0,
		peoplePerWeek:0,
		amountOfWeeks:0
	})

	useEffect(() => {

		getProducts(); //Get products from DB

		let cart = JSON.parse(localStorage.getItem('cart')); //Update cart icon number
		!cart? setCartItemCount(0):setCartItemCount(cart.length);

	
		let timeoutId = null; // Resize Grid Alternating below 600px screen width to single column
		const resizeListener = () => {	
			clearTimeout(timeoutId);
			timeoutId = setTimeout(() => setWidth(getWidth()),150);
		}
		window.addEventListener('resize', resizeListener);
		return () => {
			window.removeEventListener('resize', resizeListener);
		}

	}, []); // eslint-disable-line react-hooks/exhaustive-deps

	// Get product information from server
	const getProducts = () => { 
		axios.get(`/products/find_all`)
		.then((res) => {
			let products = res.data.products.filter((prod => prod.addon === true));
			setProducts(products);
			setDisplayProd(true);
		})
		.catch((error) => {
			console.log(error);
		})
	} 


	// Render product text and pictures on alternating sides
	const renderProducts = () => (
		products.map((prod,idx)=> { 
			if(idx % 2 === 0) {
				return (			
				<React.Fragment>
					<img src={prod.image} alt="product related" className="gridImg" />
					<div className="productTextWrapperLeft" key={idx}>
						{renderProductText(prod.minMeals, prod.maxMeals, prod.name, prod.price, prod.description)}	
					</div>
				</React.Fragment>
				);
			} else { 
				return (			
				<React.Fragment>
					<div className="productTextWrapperRight" key={idx}>
						{renderProductText(prod.minMeals, prod.maxMeals, prod.name, prod.price, prod.description, prod)}	
					</div>
					<img src={prod.image} alt="product related" className="gridImg" />
				</React.Fragment>
			);}
		})
    )

	
	// Render product text and pictures as one column if screensize is smaller than 600px
    const renderSmallProducts = () => (
		products.map((prod,idx)=> { 
				return (			
				<React.Fragment>
					<img src={prod.image} alt="product related" className="gridImg" />
					<div className="productTextWrapperLeft" key={idx}>
						{renderProductText(prod.minMeals, prod.maxMeals, prod.name, prod.price, prod.description)}	
					</div>
				</React.Fragment>
				);
		})
    )



	// Render the product textfield including buttons for ordering
	const renderProductText = (minMeals, maxMeals, name, price, description) => {
		return (
		<>
		<Form onSubmit={handleSubmit} name={name}>
			<h2 id="productChoice" >{name}</h2>
			<p>{description}</p>
			<p>Price / person / meal: {price} sek</p>
			<Form.Field>
				<label>How many meals per week?</label>
				<Button.Group id="mealsPerWeek" onClick={handleChange}>
					{numberedButtons(minMeals, maxMeals)}
				</Button.Group>	
			</Form.Field>
			<Form.Field>
				<label>How many persons?</label>
					<Button.Group id="peoplePerWeek" onClick={handleChange}>
		     		{numberedButtons(1,4)}
				</Button.Group>
			</Form.Field>
			<Form.Field>
				<label>How many weeks would you like to order for?</label>
					<Button.Group id="amountOfWeeks" onClick={handleChange}>
		     		{numberedButtons(1,4)}
		     		{/*<Button type="button">Subscription</Button>*/}
				</Button.Group>
			</Form.Field>
			<div>
				<Button type='submit'>Add to Cart</Button>
			</div>
		</Form>		
		</>
	)}

	// Rendering specific value buttons for ordering 
	const numberedButtons = (min, max) => {
		let arr = [];	
		for(let i = min;i<=max;i++) {
			arr.push(i); 
		}
		return(arr.map((value, idx) => (<ButtonToggle value={value} key={idx}/>)
		))
	} 
	

	//Handling changes of values for a product (meals per week, people per week, amount of weeks)
	const handleChange = (event) => {
		setInputValues({ ...inputValues, [event.currentTarget.id]: !isNaN(event.target.innerText)?parseInt(event.target.innerText):event.target.innerText });
	};
	
	// Handling submit to cart, clearing of selected values, sending product to local storage and updating number of products icon in cart
	const handleSubmit = (event) => {
		event.preventDefault();
		
		let index = products.findIndex((ele) => ele.name === event.target.name);
		let price = products[index].price;
		let order = {product:event.target.name, mealsPerWeek:inputValues.mealsPerWeek, peoplePerWeek:inputValues.peoplePerWeek, amountOfWeeks:inputValues.amountOfWeeks, price:price};
		let cart = JSON.parse(localStorage.getItem('cart'));
		setClear({});
		if(order.mealsPerWeek !== 0 && order.peoplePerWeek !== 0 && order.amountOfWeeks !== 0) { 
			if(!cart) {
				localStorage.setItem('cart', JSON.stringify([order]));	
				setCartItemCount(1);
			} else { 
				localStorage.removeItem('cart');
				cart = [...cart];
				cart.push(order);
				setCartItemCount(cart.length);
				localStorage.setItem('cart', JSON.stringify(cart));	
			}
			setInputValues({mealsPerWeek:0, peoplePerWeek:0, amountOfWeeks:0});
		} else { 
			alert("Please ensure all fields are filled before adding to cart");
		}
	} 


	return (
		<div id="products">
			<section className="section">
				
				{/*** Whitespace Container ****/}
				<div className="whitespaceContainer">
					<h2>WOULD YOU LIKE SOMETHING EXTRA?</h2>
					<p>Below you can find some of our extra items such as desserts or beverages that you might want to add to your order.</p>
				</div>

				{/*** Grid Alternating ****/}
				<div className="gridWrapper">
					<h2>ADDONS</h2>
					<p>Addons can only be ordered together with a themed grocery product and not by itself.</p> 
					<div className="gridAlternating">
						{displayProd? (width >= 600?renderProducts():renderSmallProducts()):null}
					</div>
				</div>

				{/*** Whitespace Container ****/}
				<div className="whitespaceContainer">
					<h2>NO MORE PLASTICS </h2>
					<p>As of 2021 we are delivering all our products in paper or wooden packaging ensuring that we reduce our environmental impact by removing plastic packaging from our range of products.</p>
					<NavLink exact to={'/cart'}>	
						<Button color="brown" type='button'>Proceed to Cart</Button>
					</NavLink>
				</div>
			</section>
		</div>
	)
}

export default Addons;
