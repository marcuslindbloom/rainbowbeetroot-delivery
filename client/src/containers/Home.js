import React, {useEffect, useState} from 'react';
import {Button} from 'semantic-ui-react';
import { NavLink } from 'react-router-dom';

// *** Images ***
import Splash from '../images/SplashFood.jpg';
import SplashSection from'../images/pexels-tim-mossholder-2331884.jpg';
import Carbonara from'../images/penne_carbonara.png';
import Bolognese from'../images/bolognese.jpg';
import Ragu from'../images/indian_ragu.jpg';
import Pork from'../images/porkchops.jpg';
import Sallad from'../images/seafood_sallad.jpg';
import Friends from'../images/friends.jpg';
import Pizza from'../images/pizza.jpg';
import Electric from '../images/ElectricinCar.jpg';
import ShoppingCart from '../images/cart.png';


const Home = (props) => {
	
	const getWidth = () => (window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth);
	const [width, setWidth] = useState(getWidth());

	useEffect(() => {

		let timeoutId = null; // Resize Grid Alternating below 600px screen width to single column
		const resizeListener = () => {	
			clearTimeout(timeoutId);
			timeoutId = setTimeout(() => setWidth(getWidth()),150);
		}
		window.addEventListener('resize', resizeListener);
		return () => {
			window.removeEventListener('resize', resizeListener);
		}

	}, []); // eslint-disable-line react-hooks/exhaustive-deps

	const renderGrid = () => (

		<div className="gridAlternating">
			<img src={Friends} className="gridImg" alt="Driends in sunset"/>
			<div className="gridText">
				<h3>BECOME OUR FRIEND</h3>
				<p>First, register as a user with us, you will also have the opportunity to sign up for our delicious newsletter.</p>
				<NavLink exact to={'/register'}>	
					<Button size="big" color="brown">Register</Button>
				</NavLink>	
			</div>	
			<div className="gridText">
				<h3>FIND WHAT REALLY SUITES YOU!</h3>
				<p>Once, you have confirmed your details with us, browse to our product page and find your favourites.</p>
				<NavLink exact to={'/products'}>
					<Button color="brown" size="big">Products</Button>
				</NavLink>
			</div>
			<img src={Pizza} className="gridImg" alt="Pizza"/>
			<img src={ShoppingCart} className="gridImg" alt="Woman with a shopping cart at a brick wall"/>
			<div className="gridText">
				<h3>PUT YOUR ITEMS TO CART & PAY SAFELY</h3>
				<p>Select your favourite meal option, choose how many meals per week you wish to recieve and how many your are in the household. You may also make additions of beverage and desserts. Process your payment safely with Stripe and recieve a confirmation email.</p>
			</div>	
			<div className="gridText">
				<h3>ELECTRIFIED DELIVERY</h3>
				<p>We will deliver your groceries on Sunday evening with our specially made electric cars directly to your door. You will find included in delivery, recipes in both swedish and english, and a delivery list for each recipe. Now it's just to cook and enjoy! </p>
			</div>
			<img src={Electric} className="gridImg" alt="Electric lamps in a car"/>
		</div>

		)

	const renderSmallGrid = () => (
		<div className="gridAlternating">
			<img src={Friends} className="gridImg" alt="Driends in sunset"/>
			<div className="gridText">
				<h3>BECOME OUR FRIEND</h3>
				<p>First, register as a user with us, you will also have the opportunity to sign up for our delicious newsletter.</p>
				<NavLink exact to={'/register'}>	
					<Button size="big" color="brown">Register</Button>
				</NavLink>	
			</div>	
			<img src={Pizza} className="gridImg" alt="Pizza"/>
			<div className="gridText">
				<h3>FIND WHAT REALLY SUITES YOU!</h3>
				<p>Once, you have confirmed your details with us, browse to our product page and find your favourites.</p>
				<NavLink exact to={'/products'}>
					<Button color="brown" size="big">Products</Button>
				</NavLink>
			</div>
			<img src={ShoppingCart} className="gridImg" alt="Woman with a shopping cart at a brick wall"/>
			<div className="gridText">
				<h3>PUT YOUR ITEMS TO CART & PAY SAFELY</h3>
				<p>Select your favourite meal option, choose how many meals per week you wish to recieve and how many your are in the household. You may also make additions of beverage and desserts. Process your payment safely with Stripe and recieve a confirmation email.</p>
			</div>	
			<img src={Electric} className="gridImg" alt="Electric lamps in a car"/>
			<div className="gridText">
				<h3>ELECTRIFIED DELIVERY</h3>
				<p>We will deliver your groceries on Sunday evening with our specially made electric cars directly to your door. You will find included in delivery, recipes in both swedish and english, and a delivery list for each recipe. Now it's just to cook and enjoy! </p>
			</div>
		</div>
		)


	return(
		<>
			
			{/*** SPLASH ****/}	
			<img src={Splash} alt="Food background" id="splashImage" />
			
			<section className="section">
				{/*** Overlapping Box ****/}
				<div className="overlappingBox">
					<h2>NO MORE DINNER STRESS</h2>
					<p>Select one of our enticing food themes, get your groceries delivered to your door each week with prepared recipes to leave you with no stress and nutricious food every day. Have a look at our various themes suitable to your every need.</p>
					<NavLink exact to={'/products'}>
						<Button color="brown" size="big">Products</Button>
					</NavLink>
				</div>
				{/*** Whitespace Container ****/}
				<div className="whitespaceContainer">
					<h2>WHY RAINBOW BEETROOT?</h2>
					<p>We can offer you weekly deliveries of groceries serving up to six people, seven nights per week. Recipes provided are carefully crafted based on seasonality and trends by our awarded chefs. During the creative process of the recipes, nutrition is balanced with keeping our promise of a less than 30 minute cooking time, and maximising flavour for your enjoyment. We deliver once per week and you also have the option of additional products such as dessert or our famous beverage package.   </p>
				</div>

				{/*** Highlight Container ****/}
				<div className="highlightContainer">
					<h2>WHAT PEOPLE SAY ABOUT US.</h2>
					<p>"I never thought it would help us so much in our daily routines just having recipes available. We have never been dissapointed with a meal and the kids love to help us cook new recipes every day. The recipes are quick and easy to make after a full day of work, and we can focus on the family." </p>
					<p>Erica - Business Lawyer & Mom</p>
				</div>

				{/*** Grid Gallery ****/}
				<div className="gridWrapper">
					<h2>NEXT WEEKS RECIPES</h2>
					<p>Find some of our famous recipes to entice your tastebuds</p>
					<div className="gridGallery">
						<div className="gridHolder">
							<img src={Pork} className="gridGalleryImg" alt="porkchops meal" />
							<div className="gridText">
								<h4>GRILLED PORKCOPS</h4>
								<p>served with rice and cabbage salad.</p>
							</div>
						</div>
						<div className="gridHolder">
							<img src={Carbonara} className="gridGalleryImg" alt="Penne Carbonara" />
							<div className="gridText">
								<h4>CLASSIC CARBONARA</h4>
								<p>with penne pasta and smoked pancetta.</p>
							</div>
						</div>
						<div className="gridHolder">
							<img src={Ragu} className="gridGalleryImg" alt="Ragu" />
							<div className="gridText">
								<h4>INDIAN VEGETARIAN MASALA</h4>
								<p>with pilaf rice and raita.</p>
							</div>
						</div>
						<div className="gridHolder">
							<img src={Sallad} className="gridGalleryImg" alt="Seafood sallad" />
							<div className="gridText">
								<h4>SEAFOOD SALLAD</h4>
								<p>with shrimps and scallops.</p>
							</div>
						</div>
						<div className="gridHolder">
							<img src={Bolognese} className="gridGalleryImg" alt="Spaghetti bolognese" />
							<div className="gridText">
								<h4>BOLOGNESE RAGU</h4>
								<p>with handtossed spaghetti and 36 months parmesan.</p>
							</div>
						</div>	
					</div>
				</div>
				
				{/*** Splash Section Divider ****/}
				<img src={SplashSection} className="splashSection" alt="Landscape with vineyard"/>	

				{/*** Highlight Container ****/}
				<div className="highlightContainer">
					<h2>BEST IN TEST</h2>
					<p>We were awareded best in test 2021 by Best-in-Test.se for the most innovative products. In order to keep improving our famous chefs are in the kitchen testing new recipes coming this next season.</p>
				</div>

				{/*** Grid Alternating ****/}
				<div className="gridWrapper">
					<h2>HOW IT WORKS!</h2>
					<p>Really want to try us out but a bit unsure about the process, have a look below and we will explain it in detail.</p>
					{width >= 600?renderGrid():renderSmallGrid()}
				</div>

			{/*** Whitespace Container ****/}
				<div className="whitespaceContainer">
					<h2>WE TAKE PRIDE IN OUR PRODUCE</h2>
					<p>Our produce are always ecological and we strive to have a minimum of 80% local produce, when seasonality permits. Only the best produce is good enough for our customers, hence we have a new cooporation with dairy farmers in Jamtland for our special cheeses board dessert and we are proud to offer their awarded cheese selection.</p>
				</div>
			</section>

		</>
	);
}


export default Home;
