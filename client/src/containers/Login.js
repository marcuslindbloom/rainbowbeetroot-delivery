import React, { useState } from 'react';
import axios from '../config.js';
import { Form, Button, Input } from 'semantic-ui-react';

const Login = (props) => {
	const [ form, setValues ] = useState({
		email: '',
		password: ''
	});
	const [ message, setMessage ] = useState('');
	const [ displayMessage, setDisplayMessage] = useState(false);
	const handleChange = (e) => {
		setValues({ ...form, [e.target.name]: e.target.value });
	};

	const handleSubmit = async (e) => {
		e.preventDefault();
		try {
			const response = await axios.post(`/users/login`, {
				email: form.email,
				password: form.password
			});

			setMessage(response.data.message);
			setDisplayMessage(true);

			if (response.data.ok) {
				setTimeout(() => {
					props.login(response.data.token);
					props.history.push('/user-profile');
				}, 2000);
			}
		} catch (error) {
			console.log(error);
		} 
	};

	//Change format of Form to qualified semantic-ui format (labels in Input fields as an example)
	return (
		<>
		<section className="section">
			{/*** Whitespace Container ****/}
				<div className="whitespaceContainer"> 
					<h2>Login </h2>
					<p>Please enter username and password to access your account.</p>
				</div>

			{/*** Login Form ****/}	
			<Form onSubmit={handleSubmit} onChange={handleChange} className="formContainer">
				<Form.Group widths='equal'>
					<Form.Input label="Email" name="email" placeholder="Enter email address" />
					<Form.Field> 
						<label>Password</label>
						<Input type="password" name="password" placeholder="Enter password" />
					</Form.Field>
				</Form.Group>
				<Button color="brown">Login</Button>
				{displayMessage? <div className="messageBox"><h3>{message}</h3></div>:null}
			</Form>
		</section>
		</>
	);
};

export default Login;
