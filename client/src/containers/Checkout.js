import React, { useState, useEffect } from 'react';
import { useSetRecoilState } from 'recoil';
import { cartNumber } from '../state';
import axios from '../config.js';

const Checkout = (props) => {
	
	const [productsInCart, setProductsInCart] = useState([]);
	const setCartItemCount = useSetRecoilState(cartNumber);
	const [totalPrice, setTotalPrice] = useState(0);
	const token = JSON.parse(localStorage.getItem('token'));
	const [userData, setUserData] = useState({});
	const [paymentIntent, setPaymentIntent] = useState({});



	useEffect(() =>{
		let cart = JSON.parse(localStorage.getItem('cart'));
		if(!cart) {  
			setCartItemCount(0)
		} else { 
			setCartItemCount(cart.length);
			setProductsInCart(cart);
			calculateTotals(cart);
		}		
		getSessionData();			
	},[]) // eslint-disable-line react-hooks/exhaustive-deps
	
	useEffect(() => {
		if(typeof userData.email !== "undefined") { 
			makeOrder()
		}
	},[userData]) // eslint-disable-line react-hooks/exhaustive-deps

	const calculateTotals = (products) => { 
		let tPrice = 0;
		for(var prod of products) { 
			tPrice += prod.price * prod.mealsPerWeek * prod.peoplePerWeek * prod.amountOfWeeks;
		}
		setTotalPrice(tPrice);

	}

		//Get data from Stripe Session
	const getSessionData = async () => {
		try {
			const sessionId = JSON.parse(localStorage.getItem("sessionId"));
			const response = await axios.get(`/payment/checkout-session?sessionId=${sessionId}`);
			localStorage.removeItem("sessionId");
			setPaymentIntent({payment_intent:response.data.session.payment_intent});
			if(response.data.ok) { 
				fetchUserData();
			}
	} catch (error) {
	    	console.log(error);
	    	debugger;
	    }
	};


	//Get user data for order in server & confirmation email
	const fetchUserData = async () => {
		try {
			axios.defaults.headers.common['Authorization'] = token;
			const response = await axios.post(`/users/get_user_info`);
			setUserData(response.data);
		} catch (error) {
			console.log(error);
		}
	}
	
	//Post order to server
	const makeOrder = async () => {
		let date = JSON.parse(localStorage.getItem('date'));
		try {
			const response = await axios.post(`/orders/create`,{
				email:userData.email,
				firstName:userData.firstName, 
				lastName:userData.lastName, 
				deliveryAddress:userData.deliveryAddress, 
				products:productsInCart,
				totalPrice:totalPrice,
				paymentVerification:paymentIntent.payment_intent,
				orderDate:new Date(), 
				initialDeliveryDate: date,

			});
			if(response.data.ok) {
				alert('Order Successful, Thank You!')
				sendConfirmation();
			}
		} catch (error) {
			console.log(error);
		}	
	}
	//Post request for confirmation email to the server
	const sendConfirmation = async () => {
		let delDate = JSON.parse(localStorage.getItem('date'));
		let ordDate = new Date();
		let data = {
			name:`${userData.firstName} ${userData.lastName}`, 
			email:userData.email, 
			totalPrice:totalPrice, 
			paymentVerification:paymentIntent.payment_intent,
			products:productsInCart, 
			orderDate: ordDate,
			initialDeliveryDate:delDate
		}
		try{ 
			axios.post('/emails/send_confirmation', data)
			.then((response) => {
				if(response.data.ok) {
					alert("Confirmation email sent.")
					emptyBasket();
				}
			})
		} catch (error) {
			console.log(error);
		}
	}			
	// Clear cart, localStorage, cart icon count. 
	const emptyBasket = () => {
		localStorage.removeItem('cart');
		localStorage.removeItem('date');
		setCartItemCount(0);
		setProductsInCart([]);
		setTotalPrice(0);

	}


	return (
	<>			
		<section className="section">
			{/*** Whitespace Container ****/}
			<div className="whitespaceContainer">
				<h2>Shopping Cart</h2>
				<p>Order successful! An order confirmation will be sent to your email shortly.</p>
			</div>
			<p>Thank you for choosing to order your food with Rainbow Beetroot!</p>
		</section>
	</>
	);
};

export default Checkout;
