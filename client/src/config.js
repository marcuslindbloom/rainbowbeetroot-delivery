import axios from 'axios';
//const REACT_APP_STRIPE_PUBLIC_KEY = "pk_test_51JFkjdHCDfmpQwrZca4QieWmIKja2477F2tEuVlo7KUK5kd1ZEouyZ8xdwflsfbSnv8mEpuUOQPYS8B7OBPGPyNJ00MX0llFPI";

// =======  preparing to the deplyment  ========
const URL = window.location.hostname === `localhost`
            ? `http://localhost:3040` 
            : `http://159.89.101.181` 
// =============================================
const customInstance = axios.create ({
  baseURL : URL, 
  headers: {'Accept': 'application/json'}
})


export default customInstance;
