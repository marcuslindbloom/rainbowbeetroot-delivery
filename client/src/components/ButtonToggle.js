import React, { useState, useEffect } from 'react';
import { Button } from 'semantic-ui-react';
import { useRecoilValue } from 'recoil';
import { clearButtons } from '../state';

const ButtonToggle = (props) => {
  const clear = useRecoilValue(clearButtons);
  const [state, setState] = useState({});
  
  useEffect(() => { 
  	handleClear();
  }, [clear]) // eslint-disable-line react-hooks/exhaustive-deps

  const handleClick = () => setState((prevState) => ({ active: !prevState.active }));
  const handleClear = () =>  state.active === true? setState({}):null;
    
    const { active } = state;

    return (
      <Button toggle type="button" active={active} onClick={handleClick}>
        {props.value}
      </Button>
    )
}

export default ButtonToggle;