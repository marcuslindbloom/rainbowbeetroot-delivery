import React from 'react';
import { NavLink } from 'react-router-dom';
import { Button, Icon } from 'semantic-ui-react'
import { useRecoilValue } from 'recoil';
import { cartNumber } from '../state';


const LoginBar = (props) => {
	
	const cartItemsCount = useRecoilValue(cartNumber);


	return(
		<div className="login">
			{!props.isLoggedIn ? (
				[
					<NavLink  exact to={'/register'} key={0}>
						<Button inverted>Register</Button>
					</NavLink>,
					<NavLink  exact to={'/login'} key={1}>
						<Button inverted>Login</Button>
					</NavLink>,
					<NavLink exact to={'/addon'} key={2}>
						<Button inverted>
							<Icon name='shopping cart'/>
						</Button>
					</NavLink>
				]
			) : 
				[
				<React.Fragment key={3}>
				
				<NavLink exact to={'/user-profile'} key={4}>
					<Button inverted>My Page</Button>
				</NavLink>
					<Button onClick={(event) => (props.logout(event))} inverted>Log Out</Button>	
				<NavLink exact to={'/addons'} key={5}>
					<Button inverted>
						<Icon name='shopping cart'/>
					</Button>
				</NavLink>
				{cartItemsCount > 0? <p id="cartNumber">{cartItemsCount}</p>:null}

				</React.Fragment>
				]
			}	
		</div>
	)
};

export default LoginBar;