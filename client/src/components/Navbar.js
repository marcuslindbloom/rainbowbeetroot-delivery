import React from 'react';
import { NavLink } from 'react-router-dom';

const Navbar =  (props) => (
	<>
	<div id="backdrop"></div>
	<div className="navbar">
		<h1 onClick={(event) => props.closeMenu(event)}>X</h1>

		<NavLink exact to={'/'} onClick={(event) => props.closeMenu(event)}>
			Home
		</NavLink>
		<NavLink exact to={'/products'} onClick={(event) => props.closeMenu(event)}>
			Products
		</NavLink>
		<NavLink exact to={'/about-us'} onClick={(event) => props.closeMenu(event)}>
			About Us
		</NavLink>
		<NavLink exact to={'/environment'} onClick={(event) => props.closeMenu(event)}>
			Environment
		</NavLink>
		<NavLink exact to={'/contact-us'} onClick={(event) => props.closeMenu(event)}>
			Contact Us
		</NavLink>
		<NavLink exact to={'/faq'} onClick={(event) => props.closeMenu(event)}>
			FAQ
		</NavLink>		
	</div>

	</>
);

export default Navbar;

