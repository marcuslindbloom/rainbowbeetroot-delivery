import React, { useState, useEffect } from 'react';
import axios from '../config.js';

const UserData = (props) => { 

	const [userData, setUserData] = useState({});
	
	
	useEffect(()=> {
		fetchUserData();
		
	},[])
	useEffect(()=>{
		props.getEmail(userData.email);
	}, [userData]);  // eslint-disable-line react-hooks/exhaustive-deps


const fetchUserData = async (props) => { 
	const token = JSON.parse(localStorage.getItem('token'));
	try {
		axios.defaults.headers.common['Authorization'] = token;
		const response = await axios.post(`/users/get_user_info`);
		setUserData(response.data);
	} catch (error) {
		console.log(error);
	}
}

	return(
		<>	
			<div className="boxContainer">
				<h2>User Information</h2>	
				<p>Name: {userData.firstName} {userData.lastName}</p>
				<p>Delivery Address: {userData.deliveryAddress}</p>
				<p>Phone number: {userData.phone}</p>
				<p>Email: {userData.email}</p>
			</div>
		</>
	);
}
export default UserData;