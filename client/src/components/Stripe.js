import React from "react";
import Cart from "../containers/Cart";
import { StripeProvider, Elements } from "react-stripe-elements";
//import { REACT_APP_STRIPE_PUBLIC_KEY } from '../config';

const Stripe = props => {
  return (
    <StripeProvider apiKey={"pk_test_51JFkjdHCDfmpQwrZca4QieWmIKja2477F2tEuVlo7KUK5kd1ZEouyZ8xdwflsfbSnv8mEpuUOQPYS8B7OBPGPyNJ00MX0llFPI"}>
      <Elements>
        <Cart {...props} />
      </Elements>
    </StripeProvider>
  );
};

export default Stripe;
