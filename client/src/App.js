import React, { useState, useEffect } from 'react';
import axios from './config';
import { BrowserRouter as Router, Route, Redirect, Switch } from 'react-router-dom';
import { useSetRecoilState } from 'recoil';
import { cartNumber } from './state';
import { v4 as uuid} from 'uuid';
import { Helmet } from 'react-helmet'


// *** Images & Icons ***
import { Icon } from 'semantic-ui-react';

// *** Containers ***
import Footer from './containers/Footer';
import Login from './containers/Login';
import Register from './containers/Register';
import UserProfile from './containers/UserProfile';
import Home from './containers/Home';
import Products from './containers/Products';
import AboutUs from './containers/AboutUs';
import Environment from './containers/Environment';
import ContactUs from './containers/ContactUs';
import FAQ from './containers/FAQ';
import PaymentError from "./containers/Payment_error";
import Checkout from "./containers/Checkout";
import SingleProduct from "./containers/SingleProduct";
import Addons from "./containers/Addons";

// *** Components ***
import Navbar from './components/Navbar';
import LoginBar from './components/LoginBar';
import Stripe from "./components/Stripe";

// *** Styles ***
import './styles/App.css';
import 'semantic-ui-css/semantic.min.css'
 


function App() {

	//**************************************
	//		   User Log-in
	//**************************************
	const [ isLoggedIn, setIsLoggedIn ] = useState(false);
	const setCartItemCount = useSetRecoilState(cartNumber); 	
	const token = JSON.parse(localStorage.getItem('token'));
	const verify_token = async () => {
		if (token === null) return setIsLoggedIn(false);
		try {
			axios.defaults.headers.common['Authorization'] = token;
			const response = await axios.post(`/users/verify_token`);
			return response.data.ok ? setIsLoggedIn(true) : setIsLoggedIn(false);
		} catch (error) {
			console.log(error);
		}
	};

	useEffect(() => {
		verify_token();
		let cart = JSON.parse(localStorage.getItem('cart'));
		!cart? setCartItemCount(0):setCartItemCount(cart.length); 
	}, []); // eslint-disable-line react-hooks/exhaustive-deps

	const login = (token) => {
		console.log('token ===>', token);
		localStorage.setItem('token', JSON.stringify(token));
		setIsLoggedIn(true);
	};
	const logout = () => {
		localStorage.removeItem('token');
		setIsLoggedIn(false);
	};

	//**************************************
	//		  Menu / Navigation
	//**************************************

	const [ showMenu, setShowMenu] = useState(false);
	const openMenu = () => { 
		showMenu? setShowMenu(false):setShowMenu(true);
	}

	const handleClose = () => {
		setShowMenu(false);
	} 

	return (
		<div>
			<Helmet>
				{/*<!-- HTML Meta Tags -->*/}
				<title>Rainbow Beetroot</title>
				<meta name="description" content="Fictional grocery delivery company "/>

				{/*<!-- Google / Search Engine Tags -->*/}
				<meta itemprop="name" content="Rainbow Beetroot"/>
				<meta itemprop="description" content="Fictional grocery delivery company "/>
				<meta itemprop="image" content="http://159.89.101.181/static/media/SplashFood.04af3cf0.jpg"/>

				{/*<!-- Facebook Meta Tags -->*/}
				<meta property="og:url" content="http://159.89.101.181"/>
				<meta property="og:type" content="website"/>
				<meta property="og:title" content="Rainbow Beetroot"/>
				<meta property="og:description" content="Fictional grocery delivery company "/>
				<meta property="og:image" content="http://159.89.101.181/static/media/SplashFood.04af3cf0.jpg"/>

				{/*<!-- Twitter Meta Tags -->*/}
				<meta name="twitter:card" content="summary_large_image"/>
				<meta name="twitter:title" content="Rainbow Beetroot"/>
				<meta name="twitter:description" content="Fictional grocery delivery company "/>
				<meta name="twitter:image" content="http://159.89.101.181/static/media/SplashFood.04af3cf0.jpg"/>
			</Helmet>
			<Router>
			<header id="header">
				<div className="box">
					<Icon name='bars' inverted size='big' onClick={openMenu} />
				</div>
				<div className="box" id="title">
					<h1>Rainbow</h1>
					<h1 id="beetroot">Beetroot</h1>
				</div>
				<div className="box">	
					<LoginBar key={uuid()} logout={logout} isLoggedIn={isLoggedIn} />
				</div>
			</header>
			
				{showMenu? <Navbar closeMenu={handleClose}/>:null}	
				<Switch>
					<Route exact path="/" component={Home} />
					<Route exact path="/products" component={Products} />
					<Route exact path="/about-us" component={AboutUs} />
					<Route exact path="/environment" component={Environment} />
					<Route exact path="/contact-us" component={ContactUs} />
					<Route exact path="/faq" component={FAQ} />
					<Route exact path="/login" render={(props) => (isLoggedIn ? <Redirect to={'/'} /> : <Login login={login} {...props} />)}/>
					<Route path="/register" render={(props) => (isLoggedIn ? <Redirect to={'/user-profile'} /> : <Register {...props} />)}/>
					<Route path="/user-profile" render={(props) => (!isLoggedIn ? <Login login={login} {...props} /> : <UserProfile logout={logout} {...props} />)}/>
					<Route path="/cart" render={(props) => (!isLoggedIn ? <Login login={login} {...props} /> : <Stripe logout={logout} {...props} />)} />
					<Route path="/addons" render={(props) => (!isLoggedIn ? <Login login={login} {...props} /> : <Addons logout={logout} {...props} />)} />
					<Route exact path="/checkout" render={props => <Checkout {...props} />}/>					
					<Route exact path="/payment/error" render={props => <PaymentError {...props} />}/>
					<Route exact path="/product/:product" component={SingleProduct} />
				</Switch>
			</Router>
			<Footer />
		</div>
	);
}

export default App;
