const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
	email: { type: String, unique: true, required: true },
	password: { type: String, required: true }, 
	address: {type:String, required:true},
	phone: {type:String, required:true},
	admin: {type:Boolean, required:true},
	firstName:{type:String, required:true},
	lastName:{type:String, required:true}
});
module.exports = mongoose.model('users', userSchema);
