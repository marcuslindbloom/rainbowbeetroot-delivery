const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({
	name:{type:String, unique:true, required:true},
	price:{type:Number, required:true}, 
	minMeals:{type:Number, required:true},
	maxMeals:{type:Number, required:true},
	description:{type:String, required:true}, 
	longDescription:{type:String, required:true},
	image:{type:String, required:true},
	addon:{type:Boolean, required:true},

});
module.exports = mongoose.model('products', productSchema);