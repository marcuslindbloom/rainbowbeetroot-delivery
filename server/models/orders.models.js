const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({
	email:{type:String, required:true}, 
	firstName:{type:String, required:true}, 
	lastName:{type:String, required:true}, 
	deliveryAddress:{type:String, required:true}, 
	products:{type:Array, required:true}, 
	totalPrice:{type:Number, required:true}, 
	paymentVerification:{type:String, required:true}, 
	orderDate:{type:Date, required:true}, 
	initialDeliveryDate:{type:Date, required:true},
});
module.exports = mongoose.model('orders', orderSchema); 