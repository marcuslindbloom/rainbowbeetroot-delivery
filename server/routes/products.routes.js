const router = require('express').Router();
const controller = require('../controllers/products.controllers');

router.post('/create', controller.create);
router.post('/delete', controller.remove);
router.post('/update', controller.update);
router.get('/find_all', controller.findAll);
router.post('/find_one', controller.findOne);

module.exports = router;