const nodemailer = require('nodemailer')
// selecting mail service and authorazing with our credentials
const transport = nodemailer.createTransport({
// you need to enable the less secure option on your gmail account
// https://myaccount.google.com/lesssecureapps?pli=1
	service: 'Gmail',
	auth: {
		user: process.env.NODEMAILER_EMAIL,
		pass: process.env.NODEMAILER_PASSWORD,
	}
});
const send_email = async (req,res) => {
	  const { name , email, subject , message } = req.body
	  const default_subject = 'This is a default subject'
	  const mailOptions = {
	  	// to: field is the destination for this outgoing email, your admin email for example
		    to: process.env.NODEMAILER_EMAIL,
		    subject: "New message from " + name,
		    html: '<p>'+(subject || default_subject)+ '</p><p><pre>' + message + '</pre></p>'
	   }
      try{
           const response = await transport.sendMail(mailOptions)
           console.log('=========================================> Email sent !!')
           return res.json({ok:true,message:'email sent'})
      }
      catch( err ){
           return res.json({ok:false,message:err})
      }
}

const send_confirmation = async (req,res) => {
	  const { name , email, totalPrice, paymentVerification, products, orderDate, initialDeliveryDate } = req.body
	  let ordDate = orderDate.split("T");
	  let delDate = initialDeliveryDate.split("T");
	  const message = 'Thank you ' + name + ' for ordering from us at Rainbow beetroot, you have ordered the following items:'
	  const subject = "Your order from Rainbow Beetroot";
	  let order = "";
	  for(let prod of products) {
	  	order = order + '<p>' + prod.product + ' meals per week: ' + prod.mealsPerWeek + ' people per week: ' + prod.peoplePerWeek + ' amount of weeks: ' + prod.amountOfWeeks + '</p>'; 
	  }


	  const mailOptions = {
	  	// to: field is the destination for this outgoing email, your admin email for example
		    to: email,
		    subject: subject,
		    html: '<h2>Your order from Rainbow Beetroot</h2><p><pre>' + message + '</pre></p>' + order + '<p>Total amount paid: ' + totalPrice + '</p>' + '<p>Payment Verification Code: ' + paymentVerification + '</p>' + '<p>Order placed on ' + ordDate[0] + ' with first weekly delivery on ' + delDate[0] + '</p>'      
	   }
      try{
           const response = await transport.sendMail(mailOptions)
           console.log('=========================================> Email sent !!')
           return res.json({ok:true,message:'email sent'})
      }
      catch( err ){
           return res.json({ok:false,message:err})
      }
}

module.exports = { send_email, send_confirmation}