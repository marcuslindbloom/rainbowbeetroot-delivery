const Orders = require('../models/orders.models');


const create = async (req,res) => {
	const {email, firstName, lastName, products, totalPrice, deliveryAddress, paymentVerification, orderDate, initialDeliveryDate} = req.body;
	try { 
		const newOrder = {
			email,
			firstName, 
			lastName, 
			deliveryAddress, 
			products,
			totalPrice,
			paymentVerification,
			orderDate, 
			initialDeliveryDate
		}
		await Orders.create(newOrder);
		res.status(200).json({ ok: true, message: 'Order Successful' });

	} catch (error) { 
		debugger;
		console.log(error)
		res.status(400).json({ ok: false, error }); 
	}
}

const findUserOrders = async (req,res) => { 
	const { inputEmail } = req.body;
	try{
			const orders = await Orders.find({email:inputEmail});
			res.status(200).json({ ok: true, orders});
		} catch (e) { 
			res.status(400).json({ ok: false, error });
		}
}

module.exports = { create, findUserOrders };//, remove, update, findAll, findOne, findUserOrders


// router.post('/create', controller.create);
// router.post('/delete', controller.remove);
// router.post('/update', controller.update);
// router.get('/find_all', controller.findAll);
// router.post('/find_one', controller.findOne);
// routes.post('/find_user_orders', controller.findUserOrders
