const stripe = require("stripe")(process.env.STRIPE_SECRET_KEY);

const create_checkout_session = async (req, res) => {
  
  try {
    const domainURL = process.env.DOMAIN;
    const { products, email} = req.body;
    if (products.length < 1 || !products)
      return res.send({ok: false, message: "Please select at least 1 product"});
  products.forEach(item => {
      item.currency = process.env.CURRENCY;
      item.amount *= 100;
  });

    // Create new Checkout Session for the order
    session = await stripe.checkout.sessions.create({
      payment_method_types: process.env.PAYMENT_METHODS.split(", "),
      line_items: products,
      customer_email:email,
      success_url: `${domainURL}/checkout?session_id={CHECKOUT_SESSION_ID}`,
      cancel_url: `${domainURL}/payment/error`
  });
    return res.send({ ok: true, sessionId: session.id });
} catch (error) {
  console.log("ERROR =====>", error.raw.message);
  return res.send({ ok: false, message: error.raw.message });
}
};

const checkout_session = async (req, res) => {
  try {
    const { sessionId } = req.query;
    const session = await stripe.checkout.sessions.retrieve(sessionId);
    const customer = await stripe.customers.retrieve(session.customer);
    return res.send({ ok: true, session: session, customer });
} catch (error) {
    console.log("ERROR =====>", error);
    return res.send({ ok: false, message: error });
}
};

module.exports = {
  create_checkout_session,
  checkout_session
};
