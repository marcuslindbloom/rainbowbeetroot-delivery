const Product = require('../models/products.models');


const create = async (req,res) => {
	const {name, price, minMeals, maxMeals, description, longDescription, image, addon} = req.body;
	if(price <= 0) return res.status(400).json({ ok: false, message: 'please provide price above 0' });
	try {
		const product = await Product.findOne({name});
		if(product) return res.status(400).json({ok:false, message: 'product name already in use'});
		const newProduct = {
			name,
			price, 
			minMeals, 
			maxMeals,
			description, 
			longDescription, 
			image, 
			addon
		}
		await Product.create(newProduct);
		res.status(200).json({ ok: true, message: 'successfully registered' });

	} catch (error) { 
		res.status(400).json({ ok: false, error });
	}
}

const remove = async (req,res) => {
	const {name} = req.body;
	try {
		const product = await Product.findOne({name});
		if(!product) return res.status(400).json({ok:false, message: 'product not found'});
		let removed = await Product.deleteOne(product);	
		console.log(removed);
		res.status(200).json({ ok: true, message: `${name} successfully deleted`});
	} catch (error) { 
		res.status(400).json({ ok: false, error });
	}
}

const update = async (req,res) => {
	const {name, newName, price, minMeals, maxMeals, description, addon} = req.body;
	typeof newName === "undefined"? newName = name:null;
	
	let input = {name:newName}
	typeof price !=="undefined"? input["price"] = price:null; 
	typeof minMeals !=="undefined"? input["minMeals"] = minMeals:null;
	typeof maxMeals !=="undefined"? input["maxMeals"] = maxMeals:null;
	typeof description !=="undefined"? input["description"] = description:null;
	typeof longDescription !=="undefined"? input["longDescription"] = longDescription:null;
	typeof image !=="undefined"? input["image"] = image:null;
	typeof addon !=="undefined"? input["addon"] = addon:null;
	
	try { 
		const product = await Product.findOne({name});	
		if(!product) return res.status(400).json({ok:false, message: 'product not found'});

		let updated = await Product.updateOne({name}, input);
		res.status(200).json({ ok: true, message: `${name} successfully updated`});
	} catch (error) {
		res.status(400).json({ ok: false, error });
	}
}

const findAll = async (req,res) => {
		try{
			const products = await Product.find({});
			res.status(200).json({ ok: true, products});
		} catch (e){ 
			res.status(400).json({ ok: false, error });
		}
	}

const findOne = async (req, res) => { 
		let {product_id, name} = req.body;	
		if(typeof product_id !== "undefined") { 
			try { 
				const product = await Product.findOne({_id:product_id});
				if(!product) return res.status(400).json({ok:false, message: 'product not found'});
				res.status(200).json({ ok: true, product});
			} catch (e) { 
				res.status(400).json({ ok: false, error });

			}
		} else if(typeof name !== "undefined") { 
			try { 
				const product = await Product.findOne({name:name});
				if(!product) return res.status(400).json({ok:false, message: 'product not found'});
				res.status(200).json({ ok: true, product});
			} catch (e) { 
				res.status(400).json({ ok: false, error });
			}
		} else { 
			res.status(400).json({ ok: false, message: `Bad syntax`});
		}
	}	




module.exports = { create, remove, update, findAll, findOne };