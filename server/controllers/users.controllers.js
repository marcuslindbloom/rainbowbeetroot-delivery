const User = require('../models/users.models');
const argon2 = require('argon2'); //https://github.com/ranisalt/node-argon2/wiki/Options
const jwt = require('jsonwebtoken');
const validator = require('validator');
const jwt_secret = process.env.JWT_SECRET;

// the client is sending this body object
//  {
//     email: form.email,
//     password: form.password,
//     password2: form.password2
//	   address: form.address
//     phone: form.phone
//	   firstName: form.firstName
//	   lastName: form.lastName
//  }

const register = async (req, res) => {
	const { email, password, password2, address, phone, firstName, lastName } = req.body;
	let admin = false;
	if (!email || !password || !password2 || !address || !phone || !firstName || !lastName) return res.json({ ok: false, message: 'All fields are required' });
	if (password !== password2) return res.json({ ok: false, message: 'passwords must match' });
	if (!validator.isEmail(email)) return res.json({ ok: false, message: 'please provide a valid email' });
	if (!validator.isMobilePhone(phone)) return res.json({ ok: false, message: 'please provide a phone number' });
	try {
		const user = await User.findOne({ email });
		if (user) return res.json({ ok: false, message: 'email already in use' });
		const hash = await argon2.hash(password);
		const newUser = {
			email,
			password: hash,
			address,
			phone, 
			admin, 
			firstName,
			lastName
		};
		await User.create(newUser);
		res.status(200).json({ ok: true, message: 'successfully registered' });
	} catch (error) {
		res.status(400).json({ ok: false, error });
	}
};


// the client is sending this body object
//  {
//     email: form.email,
//     password: form.password
//  }
const login = async (req, res) => {
	const { email, password } = req.body;
	if (!email || !password) return res.json({ ok: false, message: 'All fields are required' });
	if (!validator.isEmail(email)) return res.json({ ok: false, message: 'invalid data provided' });
	try {
		const user = await User.findOne({ email });
		if (!user) return res.json({ ok: false, message: 'invalid data provided' });
		const match = await argon2.verify(user.password, password);
		if (match) {
			const token = jwt.sign(user.toJSON(), jwt_secret, { expiresIn: '1h' }); //{expiresIn:'365d'}
			res.status(200).json({ ok: true, message: 'welcome back', token, email });
		} else return res.json({ ok: false, message: 'invalid data provided' });
	} catch (error) {
		res.status(400).json({ ok: false, error });
	}
};

const verify_token = (req, res) => {
	const token = req.headers.authorization;
	jwt.verify(token, jwt_secret, (err, succ) => {
		err ? res.status(400).json({ ok: false, message: 'something went wrong' }) : res.status(200).json({ ok: true, succ });
	});
};

const get_user_info = (req,res) => {
	const token = req.headers.authorization;
	jwt.verify(token, jwt_secret, (err, succ) => {		
		err ? res.status(400).json({ ok: false, message: 'something went wrong' }) : res.status(200).json({ 
			ok: true, 
			email:succ.email,
			deliveryAddress: succ.address, 
			phone:succ.phone, 
			firstName:succ.firstName,
			lastName:succ.lastName
		});
	});
}


module.exports = { register, login, verify_token, get_user_info };


