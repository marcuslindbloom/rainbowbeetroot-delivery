const express = require('express');
const app = express();
const PORT = process.env.PORT || 3040;
const path = require('path');
require('dotenv').config();

// =============== BODY PARSER SETTINGS =====================
const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// =============== DATABASE CONNECTION =====================
const mongoose = require('mongoose');
mongoose.set('useCreateIndex', true);
async function connecting() {
	try {
		await mongoose.connect(process.env.MONGO, { useUnifiedTopology: true, useNewUrlParser: true });
		console.log('Connected to the DB');
	} catch (error) {
		console.log('ERROR: Seems like your DB is not running, please start it up !!!');
	}
}
connecting();

//================ CORS ================================
const cors = require('cors');
app.use(cors());
app.options('/sendEmail', cors())

app.use(function(req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS");

	next();
});

// =============== ROUTES ==============================
app.use('/users', require('./routes/users.routes'));
app.use('/products', require('./routes/products.routes'));
app.use('/orders', require('./routes/orders.routes'));
app.use("/payment", require("./routes/payment.routes"));
app.use('/emails', require('./routes/emails.routes'))

// =============== ASSETS ==============================

app.use(express.static(__dirname));
app.use(express.static(path.join(__dirname, '../client/build')));
app.use('/images', express.static('static'));

app.get('/*', function (req, res) {
  res.sendFile(path.join(__dirname, '../client/build', 'index.html'));
});

// =============== START SERVER =====================
app.listen(PORT, () => console.log(`Server listening on port ${PORT}`));
