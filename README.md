# Portfolio Page Marcus Lindbloom

Website of fictional grocery bag delivery company, incorporating user creation, ordering, stripe payment and email confirmation. 

## Libraries used

Client
- React.js 17
- Axios 0.19
- Recoil 0.3
- Semantic-ui-react 2
- Stripe 8

Server
- Argon2 0.19
- Express 4
- Mongoose 5
- Nodemailer 6
- Stripe 8

## Features

- NodeJs Express server with MongoDB database
- Stripe payment
- Mailer Server
- Argon2 encrypted Authentication

## Installing

'git clone https://gitlab.com/marcuslindbloom/rainbowbeetroot-delivery'
To clone the project to your chosen folder.

Run 'npm install' for both server and client. 

In server folder: 'nodemon index.js'
In client folder: 'npm start'

Client on localhost:3000
Server on localhost:3040

## Deployment

'npm build' in client to start production build
image links and product data in MongoDB Atlas, please request access if you want to host the project. 

## Versions

v.1.0 First production build

## Developers

Marcus Lindbloom(marcus.lindblom@me.com)
